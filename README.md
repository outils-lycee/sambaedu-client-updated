# SambaEdu Client Update Daemon

## Objectif du paquet

Ce dépôt permet de créer un paquet debian (testé pour l'instant sur debian 10 buster) qui permet la mise à jour automatisée des clients linux. Le principe de fonctionnement est le suivant :
 * Sur le serveur SambaEdu, un dépôt rsync propose l'ensemble des scripts de mise à jour
 * les client font périodiquement, ainsi qu'au reboot une synchronisation de ces scripts et les exécutent.
 
 Le premier avantage est de ne devoir gérer les scripts que sur le serveur.
 Le second avantage est de pouvoir utiliser des scripts dans n'importe quel langage de programmation (bash/perl/python/php) pour peu que celui ci soit sur le client
 
## Compilation du paquet

Le paquet est construit suivant les standard débian. 
Pour construire le paquet, il suffit de cloner le dépot git puis de lancer la commande

gbp buildpackage

(gbp se trouve dans le paquet git-buildpackage et permet de compiler de façon propre et automatisée un paquet debian issu d'un dépot GIT)
