#
# Regular cron jobs for the sambaedu-client-updated package
#
33 3	* * *	root	[ -x /usr/sbin/sambaedu-client-update ] && /usr/sbin/sambaedu-client-update
@reboot     root    [ -x /usr/sbin/sambaedu-client-update ] && sleep 120s && /usr/sbin/sambaedu-client-update
